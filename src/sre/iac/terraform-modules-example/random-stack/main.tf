resource "random_pet" "random" {

}

resource "local_file" "foo" {
    content  = random_pet.random.id
    filename = "/tmp/foo.bar"
}

output "random" {
  value = random_pet.random.id
}
